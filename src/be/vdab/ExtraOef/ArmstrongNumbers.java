package be.vdab.ExtraOef;

import java.util.Scanner;

public class ArmstrongNumbers {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter a  3 digit number");
        int an = keyboard.nextInt();
        int num1 = an % 10;
        int num2 = an / 10 % 10;
        int num3 = an /100 % 10;


        if(an == (int) (Math.pow(num1, 3) + Math.pow(num2, 3) + Math.pow(num3, 3))) {
            System.out.println(an + " is an armstrong number");
        }else{
            System.out.println(an + " is not an armstrong number");
        }

    }

}

