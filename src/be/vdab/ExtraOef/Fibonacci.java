package be.vdab.ExtraOef;

import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        int t1 = 0;
        int t2 = 1;
        int sum = 0;
        int n=0;

        System.out.println("tot hoe ver wil je de rij?");
        n = keyboard.nextInt();


        while (t1 <= n){
            System.out.print(t1 + " ");
            sum = t1 + t2;
            t1=t2;
            t2=sum;
        }
    }
}
