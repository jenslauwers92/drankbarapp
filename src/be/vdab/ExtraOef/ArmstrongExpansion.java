package be.vdab.ExtraOef;

import java.util.Scanner;

public class ArmstrongExpansion {
    public void StartApp(){

        int g =0;
        int startg;
        int restg;
        int result =0;
        int n=0;

        Scanner keyboard = new Scanner(System.in);
        System.out.println("Geef een getal in dat je wil checken");
        g = keyboard.nextInt();
        startg = g;

        for (;startg != 0; startg/=10, ++n);
        startg = g;

        for (;startg != 0; startg /=10)
        {
            restg = startg %10;
            result += Math.pow(restg, n);
        }
        if (result == g)
            System.out.println(g + " is een armstrong nummer");
        else
            System.out.println(g + " is geen armstrong nummer");

    }
}
