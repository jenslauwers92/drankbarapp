package be.vdab.ExtraOef;

import java.util.Scanner;


public class DrankbarApp {
    private static int countkoffie;
    private static int countbier;
    private static int countwater;
    private static int countcola;
    public static int age;
    Scanner keyboard = new Scanner(System.in);
    public static String name;



    public static void main(String[] args) {
        DrankbarApp da = new DrankbarApp();
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Hoe heet je?");
        name = keyboard.nextLine();
        System.out.println("Wat is je leeftijd?");
        age = keyboard.nextInt();
        da.startDrink();
    }
    public  void startDrink() {


        int keuze;

        do {
            System.out.println("Naam: " + name);
            System.out.println("Leeftijd: " + age);
            System.out.println("Drankenkaart:");
            System.out.println("1. Water");
            System.out.println("2. Cola");
            System.out.println("3. Koffie");
            System.out.println("4. Bier");
            System.out.println("0. Geen dorst");
            System.out.print("Uw keuze: ");

            keuze = keyboard.nextInt();



            switch (keuze) {
                case 1:
                    Water();
                    break;
                case 2:
                    Cola();
                    break;
                case 3:
                    Koffie();
                    break;
                case 4:
                    Bier();
                    break;
                case 0:
                    System.out.println("Na " + countkoffie + " koffies, "+ countbier + " biertjes, " + countcola + " Cola's en "
                            + countwater + " watertjes " + "heb je genoeg gedronken" );
                    break;
                default:
                    System.err.println("Ongeldige keuze, kies opnieuw");
            }
        } while (keuze != 0);
    }

    void Bier() {
        if(age < 18) {
            System.out.println(name + " U bent nog veel te jong voor een biertje!");
        }else{
            System.out.println(name + ", geniet van je biertje!");
            countbier ++;
        }
    }

    void Koffie() {
        System.out.println("Hier is je koffie, " + name);
        countkoffie ++;
    }

    void Cola() {
        System.out.println("Hier is je cola, " + name);
        countcola++;
    }

    void Water() {
        System.out.println("Hier is je water, " + name);
        countwater++;
    }

}


