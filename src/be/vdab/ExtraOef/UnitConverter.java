package be.vdab.ExtraOef;


import java.util.InputMismatchException;
import java.util.Scanner;

public class UnitConverter {

    Scanner keyboard = new Scanner(System.in);


    public static void main(String[] args) {
         UnitConverter uc = new UnitConverter();

        uc.startConverting();

    }
    public  void startConverting() {


        int keuze;

        do {
            System.out.println("Converteerlijst:");
            System.out.println("1. km to mijl");
            System.out.println("2. Euro to dollar");
            System.out.println("3. Celsius to Fahrenheit");
            System.out.println("4. Meters to feet");
            System.out.println("0. Genoeg geconverteerd");
            System.out.print("Uw keuze: ");

            keuze = keyboard.nextInt();


            switch (keuze) {
                case 1:
                    KmMile();
                    break;
                case 2:
                    EuroDollar();
                    break;
                case 3:
                    CelsiusFahrenheit();
                    break;
                case 4:
                    MetersFeet();
                    break;
                case 0:
                    System.out.println("Genoeg geconverteerd");
                    break;
                default:
                    System.err.println("Ongeldige keuze, kies opnieuw");
            }
        } while (keuze != 0);
    }



    private void MetersFeet() {
        float meter =0;
        while (true){

            try {

                System.out.println("Geef de afstand in meter in");
                meter = keyboard.nextFloat();
                break;
            }catch (InputMismatchException ime) {
                System.out.println("getal ingeven!");
            } finally {
                keyboard.nextLine();
            }
            }

        System.out.println("De afstand in meter: " + meter);
        float feet = (float) (meter * 3.28);
        System.out.println("De afstand in feet: " + feet + "\n");
        System.out.println("Nog eentje converteren? (1.yes/2.no)");
        int ans = keyboard.nextInt();
        if (ans == 1) {
            MetersFeet();

        } else if (ans == 2) {
            startConverting();
        } else {
            System.out.println("wrong input");
            System.out.println("Nog eentje converteren? (1.yes/2.no)");
            ans = keyboard.nextInt();
        }
    }


            private void CelsiusFahrenheit () {
                float celsius =0;
                while(true) {
                    try {

                        System.out.println("Geef de temperatuur in Celsius in");
                        celsius = keyboard.nextFloat();
                        break;
                    } catch (InputMismatchException ime) {
                        System.out.println("getal ingeven!");
                    } finally {
                        keyboard.nextLine();
                    }
                }
                System.out.println("De temperatuur bedraagt " + celsius + " graden Celsius");
                float fahrenheit = (float) (celsius * 33.8);
                System.out.println("De temperatuur bedraagt " + fahrenheit + " graden Fahrenheit");
                System.out.println("Nog eentje converteren? (1.yes/2.no)");
                int ans = keyboard.nextInt();
                if (ans == 1) {
                    CelsiusFahrenheit();

                } else if (ans == 2) {
                    startConverting();
                } else {
                    System.out.println("wrong input");
                    System.out.println("Nog eentje converteren? (1.yes/2.no)");
                    ans = keyboard.nextInt();
                }

            }

            private void EuroDollar () {
                float euro =0;
                while(true){
                    try {

                        System.out.println("Geef het bedrag in euro in");
                        euro = keyboard.nextFloat();
                        break;
                    } catch (InputMismatchException ime){
                        System.out.println("getal ingeven");
                    } finally {
                        keyboard.nextLine();
                    }
                    }
                System.out.println("Bedrag in euro: " + euro);
                float dollar = (float) (euro * 1.14);
                System.out.println("Bedrag in dollar: " + dollar);
                System.out.println("Nog eentje converteren? (1.yes/2.no)");
                int ans = keyboard.nextInt();
                if (ans == 1) {
                    EuroDollar();

                } else if (ans == 2) {
                    startConverting();
                } else {
                    System.out.println("wrong input");
                    System.out.println("Nog eentje converteren? (1.yes/2.no)");
                    ans = keyboard.nextInt();
                }


            }

            private void KmMile () {
                float km = 0;
                while (true){
                    try {
                        System.out.println("Geef een afstand in in kilometer");
                        km = keyboard.nextFloat();
                        break;
                    }catch (InputMismatchException ime){
                        System.out.println("getal ingeven!");
                    }finally {
                        keyboard.nextLine();
                    }
                    }
                System.out.println("afstand in kilometer: " + km);
                float mijl = (float) (km * 0.62137);
                System.out.println("afstand in mijl: " + mijl);
                System.out.println("Nog eentje converteren? (1.yes/2.no)");
                int ans = keyboard.nextInt();
                if (ans == 1) {
                    KmMile();

                } else if (ans == 2) {
                    startConverting();
                } else {
                    System.out.println("wrong input");
                    System.out.println("Nog eentje converteren? (1.yes/2.no)");
                    ans = keyboard.nextInt();
                }


            }


    }
